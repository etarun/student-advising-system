class DegreeProgression < ActiveRecord::Base
  belongs_to :students,
             :class_name => 'User',
             :foreign_key => 'user_id'
  
  belongs_to :course
end
