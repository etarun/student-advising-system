json.array!(@notes) do |note|
  json.extract! note, :id, :advisor_notes, :advisor_student_notes
  json.url note_url(note, format: :json)
end
